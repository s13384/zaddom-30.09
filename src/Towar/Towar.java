package Towar;

public class Towar {

	public NazwaTowaru nazwaTowaru			= new NazwaTowaru();
	public Jednostka jednostka				= new Jednostka();
	public CenaJednNetto cenaJednNetto		= new CenaJednNetto();
	public CenaJednBrutto cenaJednBrutto	= new CenaJednBrutto();
	public Ilosc ilosc						= new Ilosc();
	public CenaCalkNetto cenaCalkNetto		= new CenaCalkNetto();
	public CenaCalkBrutto cenaCalkBrutto	= new CenaCalkBrutto();
	public ProcentPodatku procentPodatku	= new ProcentPodatku();
	
	public Towar()
	{
		
	}
	public Towar(String nazwaTowaru, String jednostka, double cenaJednNetto, double cenaJednBrutto, int ilosc)
	{
		this.nazwaTowaru.setNazwaTowaru(nazwaTowaru);
		this.jednostka.setJednostka(jednostka);
		this.cenaJednNetto.setCenaJednNetto(cenaJednNetto);
		this.cenaJednBrutto.setCenaJednBrutto(cenaJednBrutto);
		
		this.cenaCalkNetto.setCenaCalkNetto(cenaJednNetto*ilosc);
		this.cenaCalkBrutto.setCenaCalkBrutto(cenaJednBrutto*ilosc);
		this.procentPodatku.setProcentPodatku(cenaJednNetto/cenaJednBrutto);
	}
	public void wyswietlOpis()
	{
		System.out.println( "-------------------------------" );
		System.out.println( "Nazwa : " + nazwaTowaru.getNazwaTowaru() );
		System.out.println( "Ilosc : " + ilosc.getIlosc() );
		System.out.println( "Jednostka : " + jednostka.getJednostka() );
		System.out.println( "-------------------------------" );		
	}
	public void setCenaJednNetto(double cena)
	{
		cenaJednNetto.setCenaJednNetto(cena);
		cenaCalkNetto.setCenaCalkNetto(cena*ilosc.getIlosc());
	}
	public void setCenaJednBrutto(double cena)
	{
		cenaJednBrutto.setCenaJednBrutto(cena);
		cenaCalkBrutto.setCenaCalkBrutto(cena*ilosc.getIlosc());
	}
	public void setIlosc(int ilosc)
	{
		this.ilosc.setIlosc(ilosc);
		cenaCalkBrutto.setCenaCalkBrutto(ilosc*cenaJednBrutto.getCenaJednBrutto());
		cenaCalkNetto.setCenaCalkNetto(ilosc*cenaJednNetto.getCenaJednNetto());
	}
}
