package Main;

import java.util.ArrayList;
import java.util.Scanner;

import Faktura.Data;
import Faktura.Faktura;
import Faktura.NumerFaktury;
import Reszta.FakturaInfo;
import Reszta.PokazFaktury;
import Reszta.PokazTowary;
import Reszta.StworzFakture;
import Reszta.StworzTowar;
import Reszta.SzukajTowaru;
import Reszta.TowarInfo;
import Reszta.TowaryInfo;
import Reszta.UsunFakture;
import Reszta.UsunTowar;
import Towar.Towar;

public class RobProgram {

	public static void achmedOdpalaj(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		boolean program=true;
		int wybor;
		
		while (program)
		{
			if (faktury.size()==0)
			{
				System.out.println("~!~Aktualnie niema zadnych faktur");
				System.out.println("~!~Wpisz 1 by dodac fakture lub 0 by wyjsc");
				if (reader.nextInt()==1)
				{
					StworzFakture.stworzFakture(faktury);
				}
				else
				{
					program=false;
				}
				
			}
			else
			{
				PokazFaktury.pokazFaktury(faktury);
				System.out.println("By dodac fakture wcisnij 1");
				System.out.println("By usunac fakture wcisnij 2");
				System.out.println("By dodac towar do faktury wcisnij 3");
				System.out.println("By usunac towar z faktury wcisnij 4");
				System.out.println("By pokazac informacje z towarow z faktury 5");
				System.out.println("By wyjsc wpisz 0");
				wybor=reader.nextInt();
				if (wybor==1)
				{
					StworzFakture.stworzFakture(faktury);
				}
				else if (wybor==2)
				{
					UsunFakture.usunFakture(faktury);
				}
				else if (wybor==3)
				{
					StworzTowar.stworzTowar(faktury);
				}
				else if (wybor==4)
				{
					UsunTowar.usunTowar(faktury);
				}
				else if (wybor==5)
				{
					TowaryInfo.towaryInfo(faktury);
				}
				else if (wybor==0)
				{
					program=false;
				}
			}
		}
		System.out.println("Koniec programu");
		reader.close();
		/*
		faktury.add(new Faktura(new Data(1,1,1999), new Data(10,1,1999), new NumerFaktury(50)));
		faktury.add(new Faktura(new Data(1,2,1999), new Data(10,2,1999), new NumerFaktury(51)));
		faktury.add(new Faktura(new Data(1,3,1999), new Data(10,3,1999), new NumerFaktury(52)));
		faktury.get(0).getTowary().add(new Towar("papier","sztuki", 0.4, 0.5, 100));
		faktury.get(0).getTowary().add(new Towar("nozyczki","sztuki", 5, 6.4, 50));
		faktury.get(0).getTowary().add(new Towar("klej","sztuki", 3, 3.5, 200));
		
		faktury.get(1).getTowary().add(new Towar("papier2","sztuki", 0.8, 1.0, 100));
		faktury.get(1).getTowary().add(new Towar("papier3","sztuki", 10, 15, 10));
		faktury.get(1).getTowary().add(StworzTowar.stworzTowar());
		
		faktury.get(2).getTowary().add(new Towar("piasek","tony", 1000, 1200, 4));
		faktury.get(2).getTowary().add(new Towar("patyki","kilogramy", 1, 1.5, 5));
		
		SzukajTowaru.szukajTowaru(faktury.get(0), "papier");
		
		PokazTowary.pokazTowary(faktury.get(1));
		
		TowarInfo.towarInfo(faktury.get(1).getTowary().get(2));
		
		FakturaInfo.FakturaInfo(faktury.get(1));
		*/
		/*
		Faktura test = new Faktura();
		test.numerFaktury.setNumerFaktury(1234);
		
		faktury.add(test);
		System.out.print(test.towary.size());
		*/
	}
}
