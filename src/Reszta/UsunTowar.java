package Reszta;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import Faktura.Faktura;

public class UsunTowar {

	public static void usunTowar(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		String nazwaTowaru;
		int numerFaktury;
		int control=0;
		int i=0,j=0;
		
		System.out.println("Podaj nazwe towaru do usuniecia");
		nazwaTowaru=reader.nextLine();
		System.out.println("Podaj numer faktury z ktorej chcesz usunac towar");
		numerFaktury=reader.nextInt();
		
		for (i=0;i<faktury.size();i++)
		{
			if (faktury.get(i).getNumerFaktury()==numerFaktury)
			{
				break;
			}
			else control++;
		}
		if (control==faktury.size())
		{
			System.out.println("Niema faktury o numerze " + numerFaktury);
		}
		else
		{
			int tmpSize=faktury.get(i).getTowary().size();
			control=0;
			for (j=0;j<tmpSize;j++)
			{
				//System.out.println("========================");
				//System.out.println(faktury.get(i).getTowary().get(j).nazwaTowaru.getNazwaTowaru());
				//System.out.println(nazwaTowaru);
				//System.out.println("========================");
				//if (faktury.get(i).getTowary().get(j).nazwaTowaru.getNazwaTowaru()==nazwaTowaru)
				if (Objects.equals( faktury.get(i).getTowary().get(j).nazwaTowaru.getNazwaTowaru(),nazwaTowaru))
				{
					faktury.get(i).getTowary().remove(j);
					//System.out.println("USUWANIE TOWARU");
				}
				else control++;
			}
			if (control==tmpSize) System.out.println("Niema towaru o nazwie " + nazwaTowaru + " na fakturze o numerze " + 
					faktury.get(i).getNumerFaktury());
		}
		//reader.close();
	}
}
