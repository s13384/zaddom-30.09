package Reszta;

import java.util.ArrayList;
import java.util.Scanner;

import Faktura.Faktura;
import Towar.Towar;

public class StworzTowar {

	public static void stworzTowar(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		Towar towar = new Towar();
		String tmpString;
		Float tmpFloat;
		Integer tmpInt;
		int numerFkt, control=0;
		
		System.out.println("Podaj numer faktury do ktorej chcesz dodac towar");
		numerFkt=Integer.parseInt(reader.nextLine());
		
		System.out.println("Podaj nazwe towaru");
		tmpString=reader.nextLine();
		towar.nazwaTowaru.setNazwaTowaru(tmpString);
		
		System.out.println("Podaj jednostke (kilogramy/sztuki/tony/metry/centymetry)");
		tmpString=reader.nextLine();
		towar.jednostka.setJednostka(tmpString);
		
		System.out.println("Podaj ilosc");
		tmpInt=Integer.parseInt(reader.nextLine());
		towar.ilosc.setIlosc(tmpInt);
		
		System.out.println("Podaj cene netto za jednostke");
		tmpFloat=Float.parseFloat(reader.nextLine());
		towar.cenaJednNetto.setCenaJednNetto(tmpFloat);
		
		System.out.println("Podaj cene brutto za jedonstke");
		tmpFloat=Float.parseFloat(reader.nextLine());
		towar.cenaJednBrutto.setCenaJednBrutto(tmpFloat);
		
		for (int i=0;i<faktury.size();i++)
		{
			if (faktury.get(i).getNumerFaktury()==numerFkt)
			{
				faktury.get(i).addTowar(towar);
			}
			else control++;
		}
		if (control==faktury.size()) System.out.println("Niema faktury o numerze " + numerFkt);
		
		//reader.close();
	}
}
