package Reszta;

import java.util.ArrayList;
import java.util.Scanner;

import Faktura.Faktura;

public class SzukajFaktury {

	public static int szukajFaktury(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		int numerFaktury;
		int control=0;
		int i=0;
		
		System.out.println("Podaj numer faktury ");
		numerFaktury=reader.nextInt();
		
		for (i=0;i<faktury.size();i++)
		{
			if (faktury.get(i).getNumerFaktury()==numerFaktury)
			{
				break;
			}
			else control++;
		}
		//reader.close();
		if (control==faktury.size())
		{
			System.out.println("Niema faktury o numerze " + numerFaktury);
			return -1;
		}
		else
		{
			return i;
		}		
	}
}
