package Reszta;

import java.util.ArrayList;

import Faktura.Faktura;
import Towar.Towar;

public class SzukajTowaru {
	
	public static int szukajTowaru(Faktura fakturaTmp, String nazwatowaru)
	{
		ArrayList<Towar> towarlist = fakturaTmp.getTowary();
		int i;
		int found=0;
		Towar tmpTowar=null;
		for (i=0;i<towarlist.size();i++)
		{
			tmpTowar=towarlist.get(i);
			if (tmpTowar.nazwaTowaru.getNazwaTowaru() == nazwatowaru)
			{
				found=1;
				break;
			}
		}
		if (found==1)
		{
			System.out.println("Szukany towar jest na fakturze :");
			tmpTowar.wyswietlOpis();
		}
		else
		{
			System.out.println("Niema takiego towaru");
		}
		if (found==1)
		{
			return i;
		}
		else 
		{
			return -1;
		}
	}
}
