package Reszta;

import Towar.Towar;

public class TowarInfo {

	public static void towarInfo(Towar towar)
	{
		System.out.println("->Nazwa towaru : " + towar.nazwaTowaru.getNazwaTowaru());
		System.out.println("->Ilosc : " + towar.ilosc.getIlosc());
		System.out.println("->Jednostka : " + towar.jednostka.getJednostka());
		System.out.println("->Cena netto za jednostke : " + towar.cenaJednNetto.getCenaJednNetto());
		System.out.println("->Laczna cena netto : " + towar.cenaCalkNetto.getCenaCalkNetto());
		System.out.println("->Cena brutto za jednostke : " + towar.cenaJednBrutto.getCenaJednBrutto());
		System.out.println("->Laczna cena brutto : " + towar.cenaCalkBrutto.getCenaCalkBrutto());
	}
}
