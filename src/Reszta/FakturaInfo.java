package Reszta;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import Faktura.Faktura;

public class FakturaInfo {

	public static void fakturaInfo(Faktura faktura, String str)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
		DateFormat dateFormatDD = new SimpleDateFormat("dd");
		DateFormat dateFormatMM = new SimpleDateFormat("MM");
		DateFormat dateFormatRR = new SimpleDateFormat("yyyy");
		Date date = new Date();
		Date date2 = new Date();
		try {
			date2 =dateFormat.parse(Integer.toString(faktura.terminZaplatyData.getDzien()) + " " + faktura.terminZaplatyData.getMiesiac() + " " + faktura.terminZaplatyData.getRok());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(dateFormat.format(date)); 
		
		//long dniDoSplaty=RoznicaDni.getDifferenceDays(date2, date);
		long dniDoSplaty=RoznicaDni.calculateDays(Integer.toString(date.getDay()) 
				+ "/" + Integer.toString(date.getMonth()) 
				+ " " + Integer.toString(date.getYear())   ,
				
				Integer.toString(faktura.terminZaplatyData.getRok()) 
				+ "/" + Integer.toString(faktura.terminZaplatyData.getMiesiac()) 
				+ " " + Integer.toString(faktura.terminZaplatyData.getDzien())  );
		
		
		int dd,mm,rr;
		dd=Integer.parseInt(dateFormatDD.format(date));
		mm=Integer.parseInt(dateFormatMM.format(date));
		rr=Integer.parseInt(dateFormatRR.format(date));
		date.compareTo(new Date());
		
		System.out.println("++++++++++++++++++");
		
		System.out.println("TEST DATA : " + dd + "." + mm + "." + rr);
		
		System.out.println("Faktura");
		System.out.println("Numer Faktury : " + faktura.getNumerFaktury());
		System.out.println("Data Sprzedazy : " + faktura.dataSprzedazy.getDzien() + "." + 
				faktura.dataSprzedazy.getMiesiac() + "." + faktura.dataSprzedazy.getRok());
		System.out.println("Cena netto : " + faktura.cenaCalkNetto.getCenaCalkNetto());
		
		System.out.println("Termin zaplaty : " + faktura.terminZaplatyData.getDzien() + "." + 
				faktura.terminZaplatyData.getMiesiac() + "." + faktura.terminZaplatyData.getRok());
		
		System.out.println("Ilosc dni do splaty : "  + dniDoSplaty);
		
		System.out.println("Cena netto : " + faktura.cenaCalkNetto.getCenaCalkNetto());
		
		if (str != "bez")
			PokazTowary.pokazTowary(faktura);
		
		System.out.println("++++++++++++++++++");
	}
}
