package Reszta;

import java.util.ArrayList;
import java.util.Scanner;

import Faktura.Data;
import Faktura.Faktura;
import Faktura.NumerFaktury;

public class StworzFakture {

	public static void stworzFakture(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		Data data1=new Data(), data2=new Data();
		int nr;
		//System.out.println(); // data termin numer
		System.out.println("Podaj NR faktury");
		nr=reader.nextInt();
		
		System.out.println("Podaj termin splaty");
		System.out.println("Dzien : ");
		data1.setDzien(reader.nextInt());
		System.out.println("Miesiac : ");
		data1.setMiesiac(reader.nextInt());
		System.out.println("Rok : ");
		data1.setRok(reader.nextInt());
		
		System.out.println("Podaj date faktury");
		System.out.println("Dzien : ");
		data2.setDzien(reader.nextInt());
		System.out.println("Miesiac : ");
		data2.setMiesiac(reader.nextInt());
		System.out.println("Rok : ");
		data2.setRok(reader.nextInt());
		
		//reader.close();
		faktury.add(new Faktura(data1,data2,new NumerFaktury(nr)));
	}
}
