package Reszta;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class RoznicaDni {

	public static long getDifferenceDays(Date d1, Date d2) 
		{
			long diff = d2.getTime() - d1.getTime();
			return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		}
	 public static long calculateDays(String startDate, String endDate)
	    {
	        Date sDate = new Date(startDate);
	        Date eDate = new Date(endDate);
	        Calendar cal3 = Calendar.getInstance();
	        cal3.setTime(sDate);
	        Calendar cal4 = Calendar.getInstance();
	        cal4.setTime(eDate);
	        return daysBetween(cal3, cal4);
	    }
	 public static long daysBetween(Calendar startDate, Calendar endDate) {
	        Calendar date = (Calendar) startDate.clone();
	        long daysBetween = 0;
	        while (date.before(endDate)) {
	            date.add(Calendar.DAY_OF_MONTH, 1);
	            daysBetween++;
	        }
	        return daysBetween;
	    }
}
