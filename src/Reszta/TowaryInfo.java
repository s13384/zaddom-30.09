package Reszta;

import java.util.ArrayList;

import Faktura.Faktura;

public class TowaryInfo {

	public static void towaryInfo(ArrayList<Faktura> faktury)
	{
		int fakturaIndex,i;
		fakturaIndex=SzukajFaktury.szukajFaktury(faktury);
		
		FakturaInfo.fakturaInfo(faktury.get(fakturaIndex), "bez");
		
		if (fakturaIndex!=-1)
		{
			for (i=0;i<faktury.get(fakturaIndex).getTowary().size();i++)
			{
				TowarInfo.towarInfo( faktury.get(fakturaIndex).getTowary().get(i) );
			}
		}
	}
}
