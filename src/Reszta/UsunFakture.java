package Reszta;

import java.util.ArrayList;
import java.util.Scanner;

import Faktura.Faktura;

public class UsunFakture {

	public static void usunFakture(ArrayList<Faktura> faktury)
	{
		Scanner reader = new Scanner(System.in);
		int numerFaktury;
		int i;
		int control=0;
		System.out.println("Podaj numer faktury ktora chcesz usunac : ");
		numerFaktury=reader.nextInt();
		for (i=0;i<faktury.size();i++)
		{
			if (faktury.get(i).getNumerFaktury()==numerFaktury)
			{
				faktury.remove(i);
				System.out.println("Faktura zostala usunieta");
				break;
			}
			else
			{
				control++;
			}
		}
		if (control==faktury.size())
		{
			System.out.println("Niema faktury o numerze " + numerFaktury);
		}
		//reader.close();
	}
}
