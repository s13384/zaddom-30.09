package Faktura;

import java.util.ArrayList;

import Towar.Towar;
import Towar.CenaCalkBrutto;
import Towar.CenaCalkNetto;

public class Faktura {

	private ArrayList<Towar> towary = new ArrayList<Towar>();
	
	public Data dataSprzedazy 						= new Data();
	//public TerminZaplatyDni terminZaplatyDni		= new TerminZaplatyDni();
	public Data terminZaplatyData 					= new Data();
	public CenaCalkNetto cenaCalkNetto 				= new CenaCalkNetto();
	public CenaCalkBrutto cenaCalkBrutto 			= new CenaCalkBrutto();
	private NumerFaktury numerFaktury 				= new NumerFaktury();
	
	public Faktura()
	{
		
	}
	public Faktura(Data dataSprzedazy, Data terminZaplatyData, NumerFaktury numerFaktury)
	{
		this.dataSprzedazy=dataSprzedazy;
		this.terminZaplatyData=terminZaplatyData;
		this.numerFaktury=numerFaktury;
	}	
	public void addTowar(Towar towar)
	{
		cenyCalkPlus(towar);
		towary.add(towar);
	}
	public ArrayList<Towar> getTowary()
	{
		return towary;
	}
	public void removeTowar(Towar towar)
	{
		if (towary.contains(towar))
		{
			cenyCalkMinus(towar);
			towary.remove(towar);
		}
	}
	private void cenyCalkPlus(Towar towar)
	{
		cenaCalkNetto.setCenaCalkNetto(cenaCalkNetto.getCenaCalkNetto() + towar.cenaCalkNetto.getCenaCalkNetto());
		cenaCalkBrutto.setCenaCalkBrutto(cenaCalkBrutto.getCenaCalkBrutto() + towar.cenaCalkBrutto.getCenaCalkBrutto());
	}
	private void cenyCalkMinus(Towar towar)
	{
		cenaCalkNetto.setCenaCalkNetto(cenaCalkNetto.getCenaCalkNetto() - towar.cenaCalkNetto.getCenaCalkNetto());
		cenaCalkBrutto.setCenaCalkBrutto(cenaCalkBrutto.getCenaCalkBrutto() - towar.cenaCalkBrutto.getCenaCalkBrutto());
	}
	public void zmienNumerFaktury(int numer)
	{
		numerFaktury.setNumerFaktury(numer);
	}
	public int getNumerFaktury()
	{
		return numerFaktury.getNumerFaktury();
	}
}

