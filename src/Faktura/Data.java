package Faktura;

public class Data {

	private int dzien;
	private int miesiac;
	private int rok;
	
	public Data()
	{
		
	}
	public Data(int dd, int mm, int rr)
	{
		dzien=dd;
		miesiac=mm;
		rok=rr;
	}
	public void setDzien(int dzien)
	{
		this.dzien=dzien;
	}
	public void setMiesiac(int miesiac)
	{
		this.miesiac=miesiac;
	}
	public void setRok(int rok)
	{
		this.rok=rok;
	}
	public int getDzien()
	{
		return dzien;
	}
	public int getMiesiac()
	{
		return miesiac;
	}
	public int getRok()
	{
		return rok;
	}
}
