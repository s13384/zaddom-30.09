package Faktura;

public class NumerFaktury {

	private int numerFaktury;
	private int moznaZmienic;
	
	public NumerFaktury()
	{
		moznaZmienic=1;
	}
	public NumerFaktury(int numerFaktury)
	{
		this.numerFaktury=numerFaktury;
		moznaZmienic=0;
	}
	public void setNumerFaktury(int numerFaktury)
	{
		if (moznaZmienic==1)
		{
			moznaZmienic=0;
			this.numerFaktury=numerFaktury;
		}
		else
		{
			System.out.println("NIE MOZNA ZMIENIAC NUMERU FAKTURY !!!");
		}
	}
	public int getNumerFaktury()
	{
		return numerFaktury;
	}
}
